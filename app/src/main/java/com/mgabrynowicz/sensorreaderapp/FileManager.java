package com.mgabrynowicz.sensorreaderapp;

import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by RENT on 2017-01-17.
 */

public class FileManager {

    private static final String FOLDER_NAME = "accelerometer";

    private final String fileName;
    private PrintWriter printWriter;

    public FileManager() {

        fileName = String.valueOf(System.currentTimeMillis());
    }

    public void openOutputFile() {

        String folderPath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + FOLDER_NAME;

        checkAndCreateFolder(folderPath);

        try {
            FileOutputStream fos = new FileOutputStream(Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + FOLDER_NAME  + "/" + fileName + ".txt");
            printWriter = new PrintWriter(fos);


        } catch (IOException ioe) {
            Log.e(getClass().getName(), ioe.getMessage());
        }
    }

    public void saveDataToFile(float x, float y, float z) {
        StringBuilder builder = new StringBuilder();
        builder.append(x);
        builder.append(";");
        builder.append(y);
        builder.append(";");
        builder.append(z);
        builder.append(";");

        printWriter.println(builder.toString());
    }

    private void checkAndCreateFolder(String path) {

        File folderFile = new File(path);

        // czy istneje czy folder istnieje
        if (!folderFile.exists() || !folderFile.isDirectory()) {
            folderFile.mkdir();
        }
    }



    public void closeOutputFile() {
        printWriter.close();
    }

}
